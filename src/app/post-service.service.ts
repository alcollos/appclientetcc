import { Injectable } from "@angular/core";
import { Component, OnInit } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { UrlService } from "src/app/provider/url.service";
import { NavController } from "@ionic/angular";
import { Router } from "@angular/router";
import { MenuService } from "src/app/provider/menu.service";
import { ServiceUserService } from "src/app/provider/service-user.service";
import { map } from "rxjs/operators";
import { CarrinhoServiceService } from "src/app/provider/carrinho/carrinho-service.service";

@Injectable({
  providedIn: "root"
})
export class PostServiceService {
  constructor(
    public http: Http,
    public serviceUrl: UrlService,
    public nav: NavController,
    private route: Router,
    public carrinhoService: CarrinhoServiceService,
    public serviceUser: ServiceUserService,
    public menuService: MenuService
  ) {}

  postCompra(valor) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.serviceUrl.getUrlCarrinho() + "adicionarCompraCliente.php",
        valor,
        {
          headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  postSelecionado(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.serviceUrl.getUrlCarrinho() + "adicionarNotaFiscal.php",
        valor,
        {
          headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  delete(codigo) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.serviceUrl.getUrlCarrinho() + "deleteprodutoNotaFiscal.php",
        codigo,
        {
          headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res;
        })
      );
  }
}
