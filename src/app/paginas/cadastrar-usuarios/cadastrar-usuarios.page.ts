import { Component, OnInit } from "@angular/core";

import {
  Validator,
  FormBuilder,
  ReactiveFormsModule,
  FormsModule,
  Validators
} from "@angular/forms";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { map } from "rxjs/operators";
import { UrlService } from "../../provider/url.service";
import { NavController } from "@ionic/angular";
import { ServiceUserService } from "../../provider/service-user.service";
import { ActivatedRoute } from "@angular/router";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx";

@Component({
  selector: "app-cadastrar-usuarios",
  templateUrl: "./cadastrar-usuarios.page.html",
  styleUrls: ["./cadastrar-usuarios.page.scss"]
})
export class CadastrarUsuariosPage implements OnInit {
  cadastrarr: any;
  nome: any;
  endereco: any;
  telefone: any;
  email: any;
  rg: any;
  cpf: any;
  dataNascimento: any;
  password: any;
  options: any;
  "";
  arquivo: any;

  foto: any;
  Cadastrar: any;
  "";

  imageSrc: any;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public http: Http,
    private activedRoute: ActivatedRoute,
    public urlService: UrlService,
    public camera: Camera
  ) {
    this.activedRoute.queryParams.subscribe(parametros => {
      this.nome = parametros["nome"];
      this.endereco = parametros["endereco"];
      this.telefone = parametros["telefone"];
      this.rg = parametros["rg"];
      this.cpf = parametros["cpf"];
      this.password = parametros["password"];
      this.dataNascimento = parametros["dataNascimento"];
      this.email = parametros["email"];

      this.cadastrarr = this.formBuilder.group({
        nome: ["", Validators.required],
        endereco: ["", Validators.required],
        telefone: ["", Validators.required],
        email: ["", Validators.required],
        rg: ["", Validators.required],
        cpf: ["", Validators.required],
        password: ["", Validators.required],
        dataNascimento: ["", Validators.required],
        foto: ["", Validators.required]
      });
    });
  }

  cadastrarUsuario() {
    // this.urlService.alertas('Atenção', 'Preencha todos os Campos');

    this.postData(this.cadastrarr.value).subscribe(data => {
      console.log("Inserido com sucesso");
    });
  }
  postData(valor) {
    let headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.urlService.getUrlCadastro() + "cadastrarUsuario.php", valor, {
        headers: headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  atualizaArquivo(event) {
    if (event.target.files && event.target.files[0]) {
      const foto = event.target.files[0];

      const formData = new FormData();
      formData.append("foto", foto);

      this.http
        .post(this.urlService.getUrlCadastro() + "upload.php", formData)
        .subscribe(resposta => console.log(foto));
    }
  }

  cadastrarFoto() {
    // this.urlService.alertas('Atenção', 'Preencha todos os Campos');

    console.log(this.foto);
    this.postFoto(this.foto).subscribe(data => {
      console.log("Inserido com sucesso");
    });
  }
  postFoto(valor) {
    let headers = new Headers({
      "Content-Type": "multipart/form-data-encoded"
    });
    return this.http.post(this.urlService.getUrlTeste() + "upload.php", valor, {
      headers: headers,
      method: "POST"
    });
  }

  openGallery() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      targetWidth: 100,
      targetHeight: 100
    };

    this.camera.getPicture(options).then(imageData => {
      const base64image = "data:image/jpeg;base64," + imageData;
      this.foto = base64image;

      this.camera.getPicture(options).then(
        imageData => {
          this.imageSrc = imageData;
        },
        err => {
          this.foto = this.imageSrc;
        }
      );
      this.urlService.alertas("oi", "oi" + this.foto);
    });
  }

  ngOnInit() {}
}
