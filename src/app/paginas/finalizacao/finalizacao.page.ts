import { Component, OnInit } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";

import { NavController, AlertController } from "@ionic/angular";
import { Router } from "@angular/router";

import { map } from "rxjs/operators";

import { CarrinhoServiceService } from "src/app/provider/carrinho/carrinho-service.service";
import { ServiceUserService } from "src/app/provider/service-user.service";
import { UrlService } from "src/app/provider/url.service";
import { PostServiceService } from "src/app/post-service.service";
import { MenuService } from "src/app/provider/menu.service";

@Component({
  selector: "app-finalizacao",
  templateUrl: "./finalizacao.page.html",
  styleUrls: ["./finalizacao.page.scss"]
})
export class FinalizacaoPage implements OnInit {
  CarrinhoItem: Array<{
    codigoCliente: any;
    codigoProd: any;
    codigoCarrinho: any;
    selecionados: any;
    // Variaveis utilizadas para captar qual o valor que foi selecionado no select
    corSelecionado: any;
    quantidadeSelecionado: any;
    tamanhoSelecionado: any;
    valorTotal: any;
    quantidade: any;
    cor: any;
    categorias: any;
    fornecedores: any;
    marcas: any;
    modelos: any;
    palmilhas: any;
    pesos: any;
    tamanhos: any;
    promocoes: any;
    valor: any;
    qtd: any;
    descricao: any;
    foto: any;
    cod_cor: any;
    cod_qtd: any;
    cod_tamanho: any;
  }>;
  valorTotal: any;
  aTamanho: Array<{
    cod_tamanho: any;
    tamanho: any;
  }>;

  aCor: Array<{
    cod_cor: any;
    cor: any;
  }>;

  paginas: any;
  detalhe: { queryParams: { paginas: any } };
  cod_cliente: any;
  constructor(
    public http: Http,
    public serviceUrl: UrlService,
    public nav: NavController,
    private route: Router,
    public carrinhoService: CarrinhoServiceService,
    public serviceUser: ServiceUserService,
    public menuService: MenuService,
    public postService: PostServiceService,
    public alert: AlertController
  ) {
    this.listCarrinho();
    this.menuService.validarMenu();
    this.listTamanho();
    this.cod_cliente = this.serviceUser.getClienteId();
    this.listCor();
  }

  listCarrinho() {
    this.CarrinhoItem = [];
    this.serviceUrl.exibirLoading();
    // tslint:disable-next-line: prefer-const
    let oi;
    this.cod_cliente = this.serviceUser.getClienteId();
    console.log(this.cod_cliente);
    oi = this.cod_cliente;
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "listFinalizacaocompra.php?idcliente=" +
          oi
      )
      .pipe(map(res => res.json()))
      .subscribe(data => {
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < data.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado

          {
            this.CarrinhoItem.push({
              codigoProd: data[i].codigo,
              codigoCliente: data[i].cliente,
              codigoCarrinho: data[i].codCarrinho,
              modelos: data[i].modelo,
              quantidade: data[i].quantidade,
              valor: data[i].valor,
              foto: data[i].foto,
              qtd: data[i].quantidade,
              descricao: data[i].descricao,
              cor: data[i].cor,
              categorias: data[i].categoria,
              fornecedores: data[i].fornecedor,
              marcas: data[i].marca,
              palmilhas: data[i].palmilha,
              pesos: data[i].peso,
              tamanhos: data[i].tamanho,
              promocoes: data[i].promocao,
              selecionados: data[i].selecionado,
              cod_tamanho: data[i].codTamanho,
              cod_cor: data[i].codCor,
              cod_qtd: data[i].codQtd,
              valorTotal: data[i].valorTotal,

              // Variaveis utilizadas para captar qual o valor que foi selecionado no select
              tamanhoSelecionado: data[i].selecao,
              corSelecionado: data[i].selecao,
              quantidadeSelecionado: data[i].selecao
            });
            this.valorTotal =
              this.CarrinhoItem[i].qtd * this.CarrinhoItem[i].valor;
            this.CarrinhoItem[i].selecionados = false;
          }
        }

        // tslint:disable-next-line: no-unused-expression
        error => {
          this.serviceUrl.alertas(
            "Atenção",
            "não possível carregar os produtos, verifique sua conexão!"
          );
          this.serviceUrl.fecharLoding();
        };
      });

    this.serviceUrl.fecharLoding();
  }

  cancelarPedido() {
    this.deleteCompra(this.cod_cliente).subscribe(data => {});
    this.route.navigate(["tabs/produtos"], this.cod_cliente);
  }

  deleteCompra(codigo) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.serviceUrl.getUrlCarrinho() + "cancelarPedidoSelecao.php",
        codigo,
        {
          headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res;
        })
      );
  }

  deleteProduto(produto) {
    this.postService.delete(produto.codigoProd).subscribe(
      data => {
        console.log("produto deletado com sucesso");
      },
      error => {
        console.log("erro ao tentar excluir" + error);
      }
    );
  }

  adicionarItem(dados) {
    console.log(this.CarrinhoItem);

    this.postService.postSelecionado(dados).subscribe(
      data => {
        console.log("Inserido com sucesso");
      },
      err => {
        console.log("erro ao tentar inserir");
      }
    );
  }

  adicionarCompra() {
    this.postService.postCompra(this.serviceUser.getClienteId()).subscribe(
      data => {
        console.log("Inserido com sucesso");
      },
      err => {
        console.log("erro ao tentar inserir");
      }
    );
  }

  selecionarItem(produto, valor) {
    switch (produto.selecionados) {
      case true:
        produto.selecionados = "true";
        this.adicionarItem(valor);

        break;
      case false:
        produto.selecionados = "false";
        this.deleteProduto(valor);
    }
    console.log(this.CarrinhoItem);
  }

  doRefresh(event) {
    console.log("Begin async operation");
    this.listCarrinho();
    this.listTamanho();
    this.listCor();

    setTimeout(() => {
      console.log("Async operation has ended");
      event.target.complete();
      console.log(event.target.complete());
    }, 2000);
  }

  listTamanho() {
    console.log(this.aTamanho);
    this.aTamanho = [];
    this.http
      .get(this.serviceUrl.getUrlSelect() + "listTamanho.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.aTamanho.push({
            cod_tamanho: dados[i].codigo,
            tamanho: dados[i].tamanho
          });
        }
      });
  }

  listCor() {
    this.aCor = [];
    this.http
      .get(this.serviceUrl.getUrlSelect() + "listCor.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.aCor.push({
            cod_cor: dados[i].codigo,
            cor: dados[i].cor
          });
        }
      });
  }

  async finalizarCompra(produtos) {
    produtos = this.serviceUser.getClienteId();
    this.carrinhoService.postCompra(produtos).subscribe(async dados => {
      if (dados.msg.produto === "existe") {
        this.adicionarCompra();
        this.detalhe = {
          queryParams: {
            paginas: 1
          }
        };
        this.route.navigate(["codigoCliente"], this.detalhe);
      } else {
        this.serviceUrl.alertas(
          "ATENÇÃO !!",
          "Você não selecionou nenhum produto"
        );
      }
    });
  }

  ngOnInit() {}
}
