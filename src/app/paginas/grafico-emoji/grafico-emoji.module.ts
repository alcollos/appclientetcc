import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GraficoEmojiPage } from './grafico-emoji.page';

const routes: Routes = [
  {
    path: '',
    component: GraficoEmojiPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GraficoEmojiPage]
})
export class GraficoEmojiPageModule {}
