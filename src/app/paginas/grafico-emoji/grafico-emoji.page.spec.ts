import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficoEmojiPage } from './grafico-emoji.page';

describe('GraficoEmojiPage', () => {
  let component: GraficoEmojiPage;
  let fixture: ComponentFixture<GraficoEmojiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraficoEmojiPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficoEmojiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
