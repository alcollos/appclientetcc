import { Component, OnInit } from "@angular/core";
import {
  AlertController,
  NavController,
  LoadingController
} from "@ionic/angular";
import { UrlService } from "../../provider/url.service";

import { Http, Headers, Response } from "@angular/http";
import { ServiceUserService } from "../../provider/service-user.service";
import { ActivatedRoute } from "@angular/router";
import {
  FormBuilder,
  Validators,
  ReactiveFormsModule,
  FormGroup,
  FormControl
} from "@angular/forms";
import { map } from "rxjs/operators";
import { CarrinhoServiceService } from "src/app/provider/carrinho/carrinho-service.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  cadastrarr: FormGroup;

  authForm: FormGroup;

  emailUser: any;
  senhaUser: any;
  nomeUser: any;

  configs = {
    isSignIn: true,
    action: "Entre",
    actionChange: "Cria uma conta"
  };
  public name = new FormControl("", [
    Validators.required,
    Validators.minLength(3)
  ]);
  dadosCadastro: any;
  constructor(
    public alert: AlertController,
    public urlService: UrlService,
    public http: Http,
    public nav: NavController,
    private activedRoute: ActivatedRoute,
    public loading: LoadingController,
    public formBuilder: FormBuilder,
    public serviceUser: ServiceUserService,
    private fb: FormBuilder,
    public carrinhoService: CarrinhoServiceService
  ) {
    if (localStorage.getItem("deslogado") == "sim") {
      localStorage.setItem("deslogado", "não");
      location.reload();
    }

    if (localStorage.getItem("user_logado") != null) {
      console.log("autenticação");
      this.autenticar();
    }
  }

  ngOnInit(): void {
    // Declara que um form está sendo criado
    this.createForm();
  }
  private createForm(): void {
    this.cadastrarr = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      // Validador que atribui uma validação obrigatória, e valida um email, vendo se ele atinge as obrigações necessárias ..@.com

      senha: ["", [Validators.required, Validators.minLength(2)]]
      // Validador que atribui uma validação obrigatória, e um espaço de caracteres de 6 digitos
    });
  }

  get email(): FormControl {
    this.emailUser = this.cadastrarr.get("email");
    return this.cadastrarr.get("email") as FormControl;
  }
  get nome(): FormControl {
    this.nomeUser = this.cadastrarr.get("nome");
    return this.cadastrarr.get("nome") as FormControl;
  }

  get senha(): FormControl {
    this.senhaUser = this.cadastrarr.get("senha");
    return this.cadastrarr.get("senha") as FormControl;
  }

  changeAuthAction(): void {
    this.configs.isSignIn = !this.configs.isSignIn;

    // Cria uma constante, que irá estipular o valor já alterado, esse valor será usando nas devidas validações de alternancia
    const { isSignIn } = this.configs;

    // Define qual ação está sendo feita, caso seja verdadeiro ou falso
    this.configs.action = isSignIn ? "Entre" : "Contate um supervisor";
    this.configs.actionChange = isSignIn
      ? "Cadastre-se"
      : "Eu já tenho uma conta";

    // Adicionando o namecontrol, se não estiver na operação de entrar

    !isSignIn
      ? this.cadastrarr.addControl("nome", this.name)
      : this.cadastrarr.removeControl("nome");
  }

  async cadastrarUsuario() {
    // this.urlService.alertas('Atenção', 'Preencha todos os Campos');
    this.carrinhoService
      .verificarCadastro(this.cadastrarr.value)
      .subscribe(async data => {
        if (data.msg.produto === "existe") {
          this.urlService.alertas(
            "Desculpe",
            "Seu cadastro já existe, favor entrar na aplicação"
          );
        } else {
          this.postData(this.cadastrarr.value).subscribe(data => {});

          const alert = await this.alert.create({
            header: "Seu cadastro foi efetuado",
            message: "Clique no botão ENTRAR",
            buttons: [
              {
                text: "OK",

                handler: () => {
                  console.log("Clicou no Cancelar");
                  this.changeAuthAction();

                  //localStorage.setItem("user_logado", this.dadosCadastro);
                  //this.nav.navigateForward("tabs/produtos");
                }
              }
            ]
          });

          await alert.present();
        }
      });
  }
  postData(valor) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.urlService.getUrlCadastro() + "cadastrarUsuario.php", valor, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  async logar() {
    console.log("vamos autenticar");
    if (this.email == undefined || this.senha == undefined) {
      this.urlService.alertas("Atenção", "Preencha todos os campos!");
    } else {
      this.urlService.exibirLoading();

      this.http
        .get(
          this.urlService.getUrlLogin() +
            "loginCliente.php?email=" +
            this.email.value +
            "&senha=" +
            this.senha.value
        )
        .pipe(map(res => res.json()))
        .subscribe(
          data => {
            if (data.msg.logado === "sim") {
              if (data.dados.status === "1") {
                this.serviceUser.setClienteId(data.dados.cod_func);
                this.serviceUser.setClienteNome(data.dados.nome);
                this.serviceUser.setClienteStatus(data.dados.status);
                this.serviceUser.setClienteFoto(data.dados.foto);
                this.serviceUser.setClienteEmail(data.dados.email);

                localStorage.setItem("idCliente", data.dados.cod_func);
                localStorage.setItem("NameCliente", data.dados.nome);
                localStorage.setItem("nivelCliente", data.dados.status);
                localStorage.setItem("setClienteFoto", data.dados.foto);
                localStorage.setItem("emailCliente", data.dados.email);

                this.nav.navigateBack("tabs/produtos");
                this.urlService.fecharLoding();
                localStorage.setItem("user_logado", data);
                console.log(data);
              } else {
                this.urlService.fecharLoding();
                this.urlService.alertas("Atenção", "Usuário Bloqueado!");
              }
            } else {
              this.urlService.fecharLoding();
              this.urlService.alertas(
                "Atenção",
                "Usuário ou senha incorretos!"
              );
            }
          },
          error => {
            this.urlService.fecharLoding();
            this.urlService.alertas(
              "Atenção",
              "Algo deu erro, verifique sua conexão com a internet"
            );
          }
        );
    }
  }

  autenticar() {
    this.serviceUser.setClienteId(localStorage.getItem("idCliente"));
    this.serviceUser.setClienteNome(localStorage.getItem("NameCliente"));
    this.serviceUser.setClienteStatus(localStorage.getItem("statusCliente"));
    this.serviceUser.setClienteFoto(localStorage.getItem("setClienteFoto"));
  }

  esqueciSenha() {
    this.nav.navigateForward("");
  }
}
