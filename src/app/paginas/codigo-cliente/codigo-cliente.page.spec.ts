import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodigoClientePage } from './codigo-cliente.page';

describe('CodigoClientePage', () => {
  let component: CodigoClientePage;
  let fixture: ComponentFixture<CodigoClientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodigoClientePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodigoClientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
