import { Component, OnInit, ViewChild } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { UrlService } from "src/app/provider/url.service";
import {
  NavController,
  AlertController,
  ModalController,
  IonRouterOutlet,
  Platform
} from "@ionic/angular";
import { Router, ActivatedRoute } from "@angular/router";
import { MenuService } from "src/app/provider/menu.service";
import { ServiceUserService } from "src/app/provider/service-user.service";
import { map } from "rxjs/operators";
import { FormBuilder, Validators } from "@angular/forms";
import { ModalAtendimentoPage } from "../modal-atendimento/modal-atendimento.page";

@Component({
  selector: "app-codigo-cliente",
  templateUrl: "./codigo-cliente.page.html",
  styleUrls: ["./codigo-cliente.page.scss"]
})
export class CodigoClientePage implements OnInit {
  @ViewChild(IonRouterOutlet) routerOutlet: IonRouterOutlet;
  codCliente: any;

  pagina: any;

  notas: Array<{
    cod_satis: any;
    notas: any;
    qtd_satis: any;
  }>;

  CarrinhoItem: Array<{
    codigos: any;
    entrega: any;
    codCliente: any;
  }>;

  notasPesquisa: any;
  checkEmoji: any;
  pesquisa: any;
  emoji: any;
  checkEmoji1: any = false;
  checkEmoji2: any = false;
  checkEmoji3: any = false;
  checkEmoji4: any = false;
  checkEmoji5: any = false;
  emojiExiste: any;
  imagemEmojiTriste: string;
  imagemEmojiMedio: string;
  imagemEmojiFeliz: string;
  entregar = true;

  constructor(
    public http: Http,
    public serviceUrl: UrlService,
    public nav: NavController,
    private route: Router,
    public serviceUser: ServiceUserService,
    public menuService: MenuService,
    public frmBuilder: FormBuilder,
    private activedRoute: ActivatedRoute,
    public alert: AlertController,
    public modalCtrl: ModalController,
    public platform: Platform
  ) {
    this.menuService.validarMenu();
    this.preencher();
    this.codCliente = this.serviceUser.getClienteId();

    this.pesquisa = this.frmBuilder.group({
      codCliente: this.serviceUser.getClienteId(),
      notasPesquisa: ["", Validators.required]
    });
    this.codCliente = this.serviceUser.getClienteId();
    this.imagemEmojiFeliz = "../../../assets/icones/feliz2.png";
    this.imagemEmojiTriste = "../../../assets/icones/ruim2.png";
    this.imagemEmojiMedio = "../../../assets/icones/ind2.png";

    this.platform.backButton.subscribeWithPriority(0, () => {
      if (this.routerOutlet && this.routerOutlet.canGoBack()) {
        this.routerOutlet.pop();
      } else if (this.route.url === "/tabs/produtos") {
        // or if that doesn't work, try
        navigator["app"].exitApp();
      } else {
        // this.generic.showAlert("Exit", "Do you want to exit the app?", this.onYesHandler, this.onNoHandler, "backPress");
        this.serviceUrl.alertas("oi", "oi");
      }
    });
    this.atualizarChat();
  }

  async atendimento() {
    const atendimentoModal = await this.modalCtrl.create({
      component: ModalAtendimentoPage
    });
    return await atendimentoModal.present();
  }
  preencher() {
    this.activedRoute.queryParams.subscribe(parametros => {
      this.pagina = parametros["paginas"];
    });
  }

  produtoRecebido() {
    this.delete(this.codCliente).subscribe(
      data => {
        console.log("produto deletado com sucesso");
      },
      error => {
        console.log("erro ao tentar excluir" + error);
      }
    );

    this.nav.navigateForward("finalizacao");
  }

  ngOnInit() {}

  delete(codigo) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.serviceUrl.getUrlFavorito() + "deleteCliente.php", codigo, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res;
        })
      );
  }
  continueComprando() {
    this.nav.navigateForward("modal-atendimento");
  }

  async adicionarNotaNPS() {
    const alert = await this.alert.create({
      header: "Você colocou a nota: " + this.notasPesquisa,
      message: "Está correto?",
      buttons: [
        {
          text: "Não",
          role: "cancel",
          handler: () => {
            console.log("Clicou no Cancelar");
          }
        },
        {
          text: "Sim",
          handler: () => {
            this.verificarNPS(this.pesquisa.value).subscribe(dados => {
              if (dados.msg.produto === "existe") {
                this.editNotaNPS(this.pesquisa.value).subscribe(data => {});
              } else {
                this.postNotaNPS(this.pesquisa.value).subscribe(data => {});
                this.serviceUrl.alertas(
                  "Não se esqueça",
                  "Realize a sua avaliação de satisfação"
                );
              }
            });
          }
        }
      ]
    });
    await alert.present();
  }

  editNotaNPS(valor) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.serviceUrl.getUrlEstoque() + "editNotaNPS.php", valor, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  atualizarChat() {
    setInterval(() => {
      this.listCarrinho();
    }, 1000);
  }

  listCarrinho() {
    this.CarrinhoItem = [];

    this.http
      .get(
        this.serviceUrl.getUrlCarrinho() +
          "listCarrinhoSelecao.php?codCliente=" +
          this.serviceUser.getClienteId()
      )
      .pipe(map(res => res.json()))
      .subscribe(data => {
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < data.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado

          {
            this.CarrinhoItem.push({
              codigos: data[i].codigo,

              codCliente: this.serviceUser.getClienteId(),

              entrega: data[i].entregue
            });

            if (this.CarrinhoItem[i].entrega === "sim") {
              this.entregar = false;
            } else {
              this.entregar = true;
            }
          }
        }
        // tslint:disable-next-line: no-unused-expression
        error => {
          this.serviceUrl.alertas(
            "Atençao",
            "Ocorreu um erro com o seu pedido. Favor verificar no caixa"
          );
          this.serviceUrl.fecharLoding();
        };
      });
  }

  cancelarPedido() {
    this.deleteCompra(this.codCliente).subscribe(data => {});
    this.route.navigate(["tabs/produtos"], this.codCliente);
  }

  deleteCompra(codigo) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.serviceUrl.getUrlCarrinho() + "cancelarPedido.php", codigo, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res;
        })
      );
  }

  postNotaNPS(valor) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.serviceUrl.getUrlEstoque() + "adicionarNotaNPS.php", valor, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  editNotaCES() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "changeEmojiCESTrue.php?emojiSelecionado=" +
          this.emoji +
          "&codCliente=" +
          this.codCliente
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.emojiExiste = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }

  /* tirarNotaCES() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "changeEmojiCESFalse.php?emojiSelecionado=" +
          this.emoji +
          "&codCliente=" +
          this.codCliente
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.emojiExiste = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }

  async verificarEmoji1() {
    this.emoji = 1;

    const alert = await this.alert.create({
      header: "Você votou no Emoji Triste",
      message: "Está correto?",
      buttons: [
        {
          text: "Não",
          role: "cancel",
          handler: () => {
            console.log("Clicou no Cancelar");
          }
        },
        {
          text: "Sim",
          handler: () => {
            switch (this.checkEmoji1) {
              case true:
                this.checkEmoji1 = "true";
                this.verificarCES(this.checkEmoji1).subscribe(data => {
                  if (data.msg.produto === "existe") {
                    this.adicionarNotaCES();
                  } else {
                    this.editNotaCES();
                  }
                  this.imagemEmojiTriste = "../../../assets/icones/ruim1.png";
                });

                break;
              case false:
                this.checkEmoji1 = "false";
                this.imagemEmojiTriste = "../../../assets/icones/ruim2.png";

                this.tirarNotaCES();
            }
            this.nav.navigateForward("perfil-usuarios");
          }
        }
      ]
    });
    await alert.present();
  }

  async verificarEmoji2() {
    this.emoji = 2;
    const alert = await this.alert.create({
      header: "Você votou no Emoji Triste",
      message: "Está correto?",
      buttons: [
        {
          text: "Não",
          role: "cancel",
          handler: () => {
            console.log("Clicou no Cancelar");
          }
        },
        {
          text: "Sim",
          handler: () => {
            switch (this.checkEmoji2) {
              case true:
                this.checkEmoji2 = "true";
                this.verificarCES(this.emoji).subscribe(data => {
                  if (data.msg.produto === "existe") {
                    this.adicionarNotaCES();
                  } else {
                    this.editNotaCES();
                  }
                  this.imagemEmojiMedio = "../../../assets/icones/ind1.png";
                });

                break;
              case false:
                this.checkEmoji2 = "false";
                this.imagemEmojiMedio = "../../../assets/icones/ind2.png";

                this.tirarNotaCES();
            }
            this.nav.navigateForward("perfil-usuarios");
          }
        }
      ]
    });
    await alert.present();
  }

  async verificarEmoji3() {
    this.emoji = 3;

    const alert = await this.alert.create({
      header: "Você votou no Emoji Triste",
      message: "Está correto?",
      buttons: [
        {
          text: "Não",
          role: "cancel",
          handler: () => {
            console.log("Clicou no Cancelar");
          }
        },
        {
          text: "Sim",
          handler: () => {
            switch (this.checkEmoji3) {
              case true:
                this.checkEmoji3 = "true";
                this.verificarCES(this.emoji).subscribe(data => {
                  if (data.msg.produto === "existe") {
                    this.adicionarNotaCES();
                  } else {
                    this.editNotaCES();
                  }
                  this.imagemEmojiFeliz = "../../../assets/icones/feliz1.png";
                });
                break;
              case false:
                this.checkEmoji3 = "false";
                this.imagemEmojiFeliz = "../../../assets/icones/feliz2.png";

                this.tirarNotaCES();
                break;
            }
            this.nav.navigateForward("perfil-usuarios");
          }
        }
      ]
    });
    await alert.present();
  }


  /* verificarEmoji4() {
    this.emoji = 4;
    switch (this.checkEmoji4) {
      case true:
        this.checkEmoji4 = "true";
        this.verificarCES();
        if (this.emojiExiste !== 0) {
          this.adicionarNotaCES();
        } else {
          this.editNotaCES();
        }
        //this.imagemCarrinho = "../../../assets/car-outline.png";
        break;
      case false:
        this.checkEmoji4 = "false";
        //this.imagemCarrinho = "../../../assets/car-black.png";

        this.tirarNotaCES();
    }
  }
  verificarEmoji5() {
    this.emoji = 5;
    switch (this.checkEmoji5) {
      case true:
        this.checkEmoji5 = "true";
        this.verificarCES();
        if (this.emojiExiste !== 0) {
          this.adicionarNotaCES();
        } else {
          this.editNotaCES();
        }
        //this.imagemCarrinho = "../../../assets/car-outline.png";
        break;
      case false:
        this.checkEmoji5 = "false";
        //this.imagemCarrinho = "../../../assets/car-black.png";

        this.tirarNotaCES();
    }
  }*/

  verificarNPS(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.serviceUrl.getUrlEstoque() + "verificarNPS.php", valor, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }
  voltarSelecao() {
    this.nav.navigateForward("finalizacao");
  }

  /*verificarCES(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.serviceUrl.getUrlEstoque() +
          "verificarCES.php?emojiSelecionado=" +
          this.emoji +
          "&codCliente=" +
          this.codCliente,
        valor,
        {
          headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  adicionarNotaCES() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "adicionarEmojiCES.php?emojiSelecionado=" +
          this.emoji +
          "&codCliente=" +
          this.codCliente
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.emojiExiste = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }*/

  finalizarCompra() {
    this.nav.navigateForward("tabs/produtos");
  }
}
