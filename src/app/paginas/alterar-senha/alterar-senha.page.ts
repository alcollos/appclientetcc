import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import {
  AlertController,
  NavController,
  LoadingController,
  Platform
} from "@ionic/angular";
import { UrlService } from "../../provider/url.service";
import { MenuService } from "../../provider/menu.service";
import { map } from "rxjs/operators";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { NFC, Ndef } from "@ionic-native/nfc/ngx";
import { ServiceUserService } from "../../provider/service-user.service";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import {
  BarcodeScannerOptions,
  BarcodeScanner
} from "@ionic-native/barcode-scanner/ngx";
import {
  CameraPreview,
  CameraPreviewOptions,
  CameraPreviewPictureOptions
} from "@ionic-native/camera-preview/ngx";
import {
  Validators,
  FormGroup,
  FormBuilder,
  FormControl
} from "@angular/forms";
import { Subscription } from "rxjs";
import { ValueAccessor } from "@ionic/angular/dist/directives/control-value-accessors/value-accessor";
import { CarrinhoServiceService } from "src/app/provider/carrinho/carrinho-service.service";

@Component({
  selector: "app-alterar-senha",
  templateUrl: "./alterar-senha.page.html",
  styleUrls: ["./alterar-senha.page.scss"]
})
export class AlterarSenhaPage implements OnInit {
  mudarSenha: FormGroup;
  codCliente: any;
  nomeCliente: any;

  senhaCliente: any;
  confirmarSenha: any;
  senhaConfirmada: any = 0;
  constructor(
    public alert: AlertController,
    public urlService: UrlService,
    public http: Http,
    public nav: NavController,
    public loading: LoadingController,
    public serviceUser: ServiceUserService,
    public ativeRouter: ActivatedRoute,
    public serviceUrl: UrlService,
    public barcodeScanner: BarcodeScanner,
    public navController: NavController,
    public menuService: MenuService,
    public nfce: NFC,
    public frmBuilder: FormBuilder,
    public camerapreview: CameraPreview,
    public platform: Platform,
    public ndef: Ndef
  ) {
    this.codCliente = this.serviceUser.getClienteId();
    this.mudarSenha = this.frmBuilder.group({
      codCliente: this.serviceUser.getClienteId(),
      senha: ["", Validators.minLength(5)],
      senhaConfirmada: ["", Validators.minLength(5)]
    });
    this.nomeCliente = this.serviceUser.getClienteNome();
  }

  get senha(): FormControl {
    this.senhaCliente = this.mudarSenha.get("senha");
    return this.mudarSenha.get("senha") as FormControl;
  }

  verificarSenha() {
    console.log(this.senhaCliente, this.confirmarSenha);
    if (this.senha.value !== this.confirmarSenha) {
      this.senhaConfirmada = 1;
    } else {
      this.senhaConfirmada = 0;
    }
  }

  async atualizarDados() {
    const alert = await this.alert.create({
      header: "Atenção !! ",
      message: "Você realmente deseja alterar a sua senha?",
      buttons: [
        {
          text: "Não",
          role: "cancel",
          handler: () => {
            console.log("Clicou no Cancelar");
          }
        },
        {
          text: "Sim",
          handler: () => {
            this.updatePassword(this.mudarSenha.value).subscribe(data => {});
          }
        }
      ]
    });
    await alert.present();
  }

  updatePassword(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.urlService.getUrlEstoque() + "atualizarSenha.php", valor, {
        headers: headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  ngOnInit() {}
}
