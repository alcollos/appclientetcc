import { Component, OnInit } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { UrlService } from "src/app/provider/url.service";
import { NavController } from "@ionic/angular";
import { Router } from "@angular/router";
import { MenuService } from "src/app/provider/menu.service";
import { ServiceUserService } from "src/app/provider/service-user.service";
import { map } from "rxjs/operators";
import { CarrinhoServiceService } from "src/app/provider/carrinho/carrinho-service.service";

@Component({
  selector: "app-favorito",
  templateUrl: "./favorito.page.html",
  styleUrls: ["./favorito.page.scss"]
})
export class FavoritoPage implements OnInit {
  CarrinhoItem: Array<{
    codigos: any;
    codCliente: any;
    cores: any;
    categorias: any;
    fornecedores: any;
    marcas: any;
    modelos: any;
    palmilhas: any;
    pesos: any;
    tamanhos: any;
    promocoes: any;
    valor: any;
    qtd: any;
    descricao: any;
    foto: any;
  }>;

  constructor(
    public http: Http,
    public serviceUrl: UrlService,
    public nav: NavController,
    private route: Router,
    public serviceUser: ServiceUserService,
    public menuService: MenuService,
    public carrinhoService: CarrinhoServiceService
  ) {
    this.validarMenu();
    this.listCarrinho();
  }

  doRefresh(event) {
    console.log("Begin async operation");
    this.listCarrinho();

    setTimeout(() => {
      console.log("Async operation has ended");
      event.target.complete();
      console.log(event.target.complete());
    }, 2000);
  }

  validarMenu() {
    if (localStorage.getItem("user_logado") != null) {
      console.log("usuario logado");

      this.serviceUser.setClienteId(localStorage.getItem("idCliente"));
      this.serviceUser.setClienteNome(localStorage.getItem("NameCliente"));
      this.serviceUser.setClienteStatus(localStorage.getItem("statusCliente"));
    } else {
      this.nav.navigateBack("login");
    }
  }

  listCarrinho() {
    this.CarrinhoItem = [];
    this.serviceUrl.exibirLoading();

    this.http
      .get(this.serviceUrl.getUrlFavorito() + "listProdutosFAVORITOS.php")
      .pipe(map(res => res.json()))
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          if (data[i].cliente === this.serviceUser.idCliente) {
            this.CarrinhoItem.push({
              codigos: data[i]["codigo"],
              modelos: data[i]["modelo"],
              valor: data[i]["valor"],
              foto: data[i]["foto"],
              qtd: data[i]["quantidade"],
              descricao: data[i]["descricao"],
              cores: data[i]["cor"],
              categorias: data[i]["categoria"],
              fornecedores: data[i]["fornecedor"],
              marcas: data[i]["marca"],
              palmilhas: data[i]["palmilha"],
              pesos: data[i]["peso"],
              codCliente: this.serviceUser.getClienteId(),
              tamanhos: data[i]["tamanho"],
              promocoes: data[i]["promocao"]
            });
          }
        }
        error => {
          this.serviceUrl.alertas(
            "Atenção",
            "não possível carregar os produtos, verifique sua conexão!"
          );
          this.serviceUrl.fecharLoding();
        };
      });

    this.serviceUrl.fecharLoding();
  }

  deleteProduto(produto) {
    console.log(produto);
    this.carrinhoService.updateFalse(produto).subscribe(data => {});
    this.carrinhoService.deleteFavorito(produto).subscribe(
      data => {
        console.log("produto deletado com sucesso");
        this.listCarrinho();
      },
      error => {
        console.log("erro ao tentar excluir" + error);
      }
    );
  }

  ngOnInit() {}
}
