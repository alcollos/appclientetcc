import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { PesquisaSatisfacaoPage } from "./pesquisa-satisfacao.page";

const routes: Routes = [
  {
    path: "",
    component: PesquisaSatisfacaoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PesquisaSatisfacaoPage]
})
export class PesquisaSatisfacaoPageModule {}
