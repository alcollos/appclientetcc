import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import {
  AlertController,
  NavController,
  LoadingController,
  Platform
} from "@ionic/angular";
import { UrlService } from "../../provider/url.service";
import { MenuService } from "../../provider/menu.service";
import { map } from "rxjs/operators";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { NFC, Ndef } from "@ionic-native/nfc/ngx";
import { ServiceUserService } from "../../provider/service-user.service";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import {
  BarcodeScannerOptions,
  BarcodeScanner
} from "@ionic-native/barcode-scanner/ngx";
import {
  CameraPreview,
  CameraPreviewOptions,
  CameraPreviewPictureOptions
} from "@ionic-native/camera-preview/ngx";
import { Validators, FormGroup, FormBuilder } from "@angular/forms";
import { Subscription } from "rxjs";
import { ValueAccessor } from "@ionic/angular/dist/directives/control-value-accessors/value-accessor";
import { CarrinhoServiceService } from "src/app/provider/carrinho/carrinho-service.service";

@Component({
  selector: "app-pesquisa-satisfacao",
  templateUrl: "./pesquisa-satisfacao.page.html",
  styleUrls: ["./pesquisa-satisfacao.page.scss"]
})
export class PesquisaSatisfacaoPage implements OnInit {
  notas: Array<{
    cod_satis: any;
    notas: any;
    qtd_satis: any;
  }>;
  codCliente: any;
  notasPesquisa: any;
  checkEmoji: any;
  pesquisa: any;
  emoji: any;
  checkEmoji1: any;
  checkEmoji2: any;
  checkEmoji3: any;
  checkEmoji4: any;
  checkEmoji5: any;
  emojiExiste: any;

  constructor(
    public alert: AlertController,
    public urlService: UrlService,
    public http: Http,
    public nav: NavController,
    public loading: LoadingController,
    public serviceUser: ServiceUserService,
    public ativeRouter: ActivatedRoute,
    public serviceUrl: UrlService,
    public barcodeScanner: BarcodeScanner,
    public navController: NavController,
    public menuService: MenuService,
    public nfce: NFC,
    public camerapreview: CameraPreview,
    public platform: Platform,
    public ndef: Ndef,
    public frmBuilder: FormBuilder,
    public carrinhoService: CarrinhoServiceService
  ) {
    this.pesquisa = this.frmBuilder.group({
      codCliente: 1, //NÃO ESQUECE IDIOTA
      notasPesquisa: ["", Validators.required]
    });
    this.codCliente = this.serviceUser.getClienteId();
    this.menuService.validarMenu();
    //this.listNotasNPS();
  }

  ngOnInit() {}

  /*listNotasNPS() {
    this.notas = [];
    this.http
      .get(this.serviceUrl.getUrlSelect() + "listNotasNPS.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.notas.push({
            cod_satis: dados[i].codigo,
            notas: dados[i].notas,
            qtd_satis: dados[i].quantidade
          });
        }
      });
  }*/

  adicionarNotaNPS() {
    this.verificarNPS(this.pesquisa.value).subscribe(dados => {
      if (dados.msg.produto === "existe") {
        this.editNotaNPS(this.pesquisa.value).subscribe(data => {});
      } else {
        this.postNotaNPS(this.pesquisa.value).subscribe(data => {});
      }
    });
  }

  editNotaNPS(valor) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.urlService.getUrlEstoque() + "editNotaNPS.php", valor, {
        headers: headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  postNotaNPS(valor) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.urlService.getUrlEstoque() + "adicionarNotaNPS.php", valor, {
        headers: headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  editNotaCES() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "changeEmojiCESTrue.php?emojiSelecionado=" +
          this.emoji +
          "&codCliente=" +
          1 // MUDAR PRORA
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.emojiExiste = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }

  tirarNotaCES() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "changeEmojiCESFalse.php?emojiSelecionado=" +
          this.emoji +
          "&codCliente=" +
          1 // MUDAR PRORA
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.emojiExiste = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
  verificarEmoji1() {
    this.verificarCES();
    this.emoji = 1;
    switch (this.checkEmoji1) {
      case true:
        this.checkEmoji1 = "true";

        if (this.emojiExiste === 0) {
          this.adicionarNotaCES();
        } else {
          this.editNotaCES();
        }
        //this.imagemCarrinho = "../../../assets/car-outline.png";
        break;
      case false:
        this.checkEmoji1 = "false";
        //this.imagemCarrinho = "../../../assets/car-black.png";

        this.tirarNotaCES();
    }
  }

  verificarEmoji2() {
    this.emoji = 2;
    switch (this.checkEmoji2) {
      case true:
        this.checkEmoji2 = "true";
        this.verificarCES();
        if (this.emojiExiste === 0) {
          this.adicionarNotaCES();
        } else {
          this.editNotaCES();
        }
        //this.imagemCarrinho = "../../../assets/car-outline.png";
        break;
      case false:
        this.checkEmoji2 = "false";
        //this.imagemCarrinho = "../../../assets/car-black.png";

        this.tirarNotaCES();
    }
  }
  verificarEmoji3() {
    this.emoji = 3;
    switch (this.checkEmoji3) {
      case true:
        this.checkEmoji3 = "true";
        this.verificarCES();
        if (this.emojiExiste === 0) {
          this.adicionarNotaCES();
        } else {
          this.editNotaCES();
        }
        //this.imagemCarrinho = "../../../assets/car-outline.png";
        break;
      case false:
        this.checkEmoji3 = "false";
        //this.imagemCarrinho = "../../../assets/car-black.png";

        this.tirarNotaCES();
    }
  }
  /* verificarEmoji4() {
    this.emoji = 4;
    switch (this.checkEmoji4) {
      case true:
        this.checkEmoji4 = "true";
        this.verificarCES();
        if (this.emojiExiste !== 0) {
          this.adicionarNotaCES();
        } else {
          this.editNotaCES();
        }
        //this.imagemCarrinho = "../../../assets/car-outline.png";
        break;
      case false:
        this.checkEmoji4 = "false";
        //this.imagemCarrinho = "../../../assets/car-black.png";

        this.tirarNotaCES();
    }
  }
  verificarEmoji5() {
    this.emoji = 5;
    switch (this.checkEmoji5) {
      case true:
        this.checkEmoji5 = "true";
        this.verificarCES();
        if (this.emojiExiste !== 0) {
          this.adicionarNotaCES();
        } else {
          this.editNotaCES();
        }
        //this.imagemCarrinho = "../../../assets/car-outline.png";
        break;
      case false:
        this.checkEmoji5 = "false";
        //this.imagemCarrinho = "../../../assets/car-black.png";

        this.tirarNotaCES();
    }
  }*/

  verificarNPS(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.serviceUrl.getUrlEstoque() + "verificarNPS.php", valor, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  verificarCES() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "verificarCES.php?emojiSelecionado=" +
          this.emoji +
          "&codCliente=" +
          1 // MUDAR PRORA
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.emojiExiste = getQuantidade.dados;
        console.log(this.emojiExiste);
      });
    this.serviceUrl.fecharLoding();
  }

  adicionarNotaCES() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "adicionarEmojiCES.php?emojiSelecionado=" +
          this.emoji +
          "&codCliente=" +
          1 // MUDAR PRORA
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.emojiExiste = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }
}
