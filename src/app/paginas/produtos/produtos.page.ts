import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import {
  AlertController,
  NavController,
  LoadingController,
  Platform
} from "@ionic/angular";
import { UrlService } from "../../provider/url.service";
import { MenuService } from "../../provider/menu.service";
import { map } from "rxjs/operators";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { NFC, Ndef } from "@ionic-native/nfc/ngx";
import { ServiceUserService } from "../../provider/service-user.service";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import {
  BarcodeScannerOptions,
  BarcodeScanner
} from "@ionic-native/barcode-scanner/ngx";
import {
  CameraPreview,
  CameraPreviewOptions,
  CameraPreviewPictureOptions
} from "@ionic-native/camera-preview/ngx";
import { Validators } from "@angular/forms";
import { Subscription } from "rxjs";
import { ValueAccessor } from "@ionic/angular/dist/directives/control-value-accessors/value-accessor";
import { CarrinhoServiceService } from "src/app/provider/carrinho/carrinho-service.service";

@Component({
  selector: "app-produtos",
  templateUrl: "./produtos.page.html",
  styleUrls: ["./produtos.page.scss"]
})
export class ProdutosPage implements OnInit {
  encodeData: any;
  scannedData: {};
  barcodeScannerOptions: BarcodeScannerOptions;
  barcodeData: any;

  picture: string;
  cameraOpts: CameraPreviewOptions = {
    x: 0,
    y: 0,
    width: window.innerWidth,
    height: window.innerHeight,
    toBack: false
  };
  cameraPictureOpts: CameraPreviewPictureOptions = {
    width: window.innerWidth,
    height: window.innerHeight,
    quality: 100
  };

  codigos: any;
  imagemCarrinho: any;
  imagemFavorito: any;

  dados: Array<{
    codigos: any;
    codCliente: any;
    cores: any;
    categorias: any;
    fornecedores: any;
    marcas: any;
    modelos: any;
    palmilha: any;
    pesos: any;
    tamanhos: any;
    promocoes: any;
    valor: any;
    qtd: any;
    cod_tamanho: any;
    descricao: any;
    foto: any;
    checkCarrinho: any;
    checkFavorito: any;
  }>;

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  aTamanho: Array<{
    cod_tamanho: any;
    tamanho: any;
  }>;
  aCor: Array<{
    codigo: any;
    cor: any;
  }>;

  quantidade: any;
  inserirQtd: any;

  constructor(
    public alert: AlertController,
    public urlService: UrlService,
    public http: Http,
    public nav: NavController,
    public loading: LoadingController,
    public serviceUser: ServiceUserService,
    public ativeRouter: ActivatedRoute,
    public serviceUrl: UrlService,
    public barcodeScanner: BarcodeScanner,
    public navController: NavController,
    public menuService: MenuService,
    public nfce: NFC,
    public camerapreview: CameraPreview,
    public platform: Platform,
    public ndef: Ndef,
    public carrinhoService: CarrinhoServiceService
  ) {
    console.log(serviceUser.getClienteId());
    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };

    this.platform.ready().then(() => {
      this.dados = [];
      this.scanNfc();
    });
    console.log("teste");
  }

  ionViewDidLoad() {
    this.startCamera();
  }

  async startCamera() {
    this.camerapreview.startCamera(this.cameraOpts).then(
      res => {
        console.log(res);
      },
      err => {
        console.log(err);
      }
    );
    const result = await this.camerapreview.startCamera(this.cameraOpts);
  }

  scanCode() {
    this.listTamanho();
    this.listCor();
    // this.listQuantidade();
    this.dados = [];
    this.imagemCarrinho = "../../../assets/car-black.png";
    this.imagemFavorito = "../../../assets/heart-outline.png";

    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedData = barcodeData;
      let nProduto;
      // tslint:disable-next-line: radix
      parseInt((nProduto = barcodeData.text));

      this.http
        .get(
          this.urlService.getUrlEstoque() +
            "detalhesProdutos.php?idproduto=" +
            nProduto
        )
        .pipe(map(res => res.json()))
        .subscribe(data => {
          // tslint:disable-next-line: prefer-for-of
          for (let i = 0; i < data.length; i++) {
            this.dados.push({
              codigos: data[i].codigo,
              modelos: data[i].modelo,
              valor: data[i].valor,
              foto: data[i].foto,
              qtd: data[i].quantidade,
              descricao: data[i].descricao,
              cores: data[i].cor,
              categorias: data[i].categoria,
              fornecedores: data[i].fornecedor,
              marcas: data[i].marca,
              palmilha: data[i].palmilha,
              pesos: data[i].peso,
              tamanhos: data[i].tamanho,
              promocoes: data[i].promocao,
              cod_tamanho: data[i].cod_tamanho,
              codCliente: this.serviceUser.getClienteId(),
              checkFavorito: data[i].checkFavoriteo,
              checkCarrinho: data[i].checkCarrinheo
            });
          }
        });
    });
  }

  code() {
    this.listTamanho();
    this.listCor();
    //this.listQuantidade();
    this.dados = [];
    this.imagemCarrinho = "../../../assets/car-black.png";
    this.imagemFavorito = "../../../assets/heart-outline.png";

    let nProduto;
    nProduto = this.codigos;

    this.http
      .get(
        this.urlService.getUrlEstoque() +
          "detalhesProdutos.php?idproduto=" +
          nProduto
      )
      .pipe(map(res => res.json()))
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          this.dados.push({
            codigos: data[i].codigo,
            modelos: data[i].modelo,
            valor: data[i].valor,
            foto: data[i].foto,
            qtd: data[i].quantidade,
            descricao: data[i].descricao,
            cores: data[i].cor,
            categorias: data[i].categoria,
            fornecedores: data[i].fornecedor,
            marcas: data[i].marca,
            palmilha: data[i].palmilha,
            pesos: data[i].peso,
            tamanhos: data[i].tamanho,
            promocoes: data[i].promocao,
            cod_tamanho: data[i].cod_tamanho,
            codCliente: this.serviceUser.getClienteId(),
            checkFavorito: data[i].checkFavoriteo,
            checkCarrinho: data[i].checkCarrinheo
          });
          console.log(this.dados[i].checkCarrinho);
        }

        console.log(this.dados);
      });
  }

  //  });

  scanNfc() {
    this.listTamanho();
    this.listCor();
    //this.listQuantidade();
    this.dados = [];
    this.imagemCarrinho = "../../../assets/car-black.png";
    this.imagemFavorito = "../../../assets/heart-outline.png";

    this.nfce
      .addNdefListener(
        () => {
          console.log("successfully attached ndef listener");
        },
        err => {
          console.log("error attaching ndef listener", err);
        }
      )
      .subscribe(nfcEvent => {
        const tag = nfcEvent.tag;
        const ndefMessage = tag.ndefMessage;

        const nProduto = this.nfce
          .bytesToString(ndefMessage[0].payload)
          .substring(3);
        // parseInt((nProduto = this.scannedData['text']));

        this.http
          .get(
            this.urlService.getUrlEstoque() +
              "detalhesProdutos.php?idproduto=" +
              nProduto
          )
          .pipe(map(res => res.json()))
          .subscribe(data => {
            for (let i = 0; i < data.length; i++) {
              this.dados.push({
                codigos: data[i].codigo,
                modelos: data[i].modelo,
                valor: data[i].valor,
                foto: data[i].foto,
                qtd: data[i].quantidade,
                descricao: data[i].descricao,
                cores: data[i].cor,
                categorias: data[i].categoria,
                fornecedores: data[i].fornecedor,
                marcas: data[i].marca,
                palmilha: data[i].palmilha,
                pesos: data[i].peso,
                tamanhos: data[i].tamanho,
                promocoes: data[i].promocao,
                checkFavorito: data[i].checkFavoriteo,
                checkCarrinho: data[i].checkCarrineho,
                cod_tamanho: data[i].cod_tamanho,
                codCliente: this.serviceUser.getClienteId()
              });
            }
          });
        //  });
      });
  }

  validarMenu() {
    if (localStorage.getItem("user_logado") != null) {
      console.log("usuario logado");

      this.serviceUser.setClienteId(localStorage.getItem("idCliente"));
      this.serviceUser.setClienteNome(localStorage.getItem("NameCliente"));
      this.serviceUser.setClienteStatus(localStorage.getItem("statusCliente"));
    } else {
      this.nav.navigateBack("login");
    }
    this.menuService.perfilMenu.push(this.menuService.menu[0]); // carrinho
    this.menuService.perfilMenu.push(this.menuService.menu[1]); // perfil
  }

  adicionarCarrinho() {
    const oi = JSON.stringify(this.dados);

    let b;

    b = oi.substring(1, oi.length - 1);

    console.log(b);
    this.postData(b).subscribe(data => {
      this.urlService.alertas(
        "Continue comprando !!",
        "O produto foi adicionado ao seu carrinho"
      );
    });
  }

  postData(valor) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http.post(
      this.serviceUrl.getUrlCarrinho() + "adicionarCarrinho.php",
      valor,
      {
        headers,
        method: "POST"
      }
    );
  }

  adicionarFavoritos() {
    const oi = JSON.stringify(this.dados);

    let b;

    b = oi.substring(1, oi.length - 1);

    console.log(b);
    this.postFavorito(b).subscribe(data => {
      this.urlService.alertas(
        "Continue comprando !!",
        "O produto foi adicionado aos seus FAVORITOS"
      );
    });
  }

  postFavorito(valor) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http.post(
      this.serviceUrl.getUrlFavorito() + "adicionarFavoritos.php",
      valor,
      {
        headers,
        method: "POST"
      }
    );
  }

  GoCarrinho() {
    this.navController.navigateForward("carrinho");
  }

  toFavoritos() {
    this.navController.navigateForward("favorito");
  }

  listTamanho() {
    console.log(this.aTamanho);
    this.aTamanho = [];
    this.http
      .get(this.serviceUrl.getUrlSelect() + "listTamanho.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.aTamanho.push({
            cod_tamanho: dados[i].codigo,
            tamanho: dados[i].tamanho
          });
        }
      });
  }

  listCor() {
    this.aCor = [];
    this.http
      .get(this.urlService.getUrlSelect() + "listCor.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        this.aCor = dados;
      });
  }

  /*listQuantidade() {
    this.aQuantidade = [];
    this.http
      .get(this.urlService.getUrlSelect() + "listQuantidade.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        this.aQuantidade = dados;
      });
  }*/

  selecionaCaracteristicasCarrinho(produto) {
    this.carrinhoService.postCaracteristicas(produto).subscribe(dados => {
      if (dados.msg.produto === "existe") {
        this.adicionarCarrinho();
      } else {
        this.serviceUrl.alertas("Desculpe", "Este produto não está disponivel");
        produto.checkCarrinho = false;
      }
    });
    console.log(this.dados);
  }

  checkCarrinho(produto) {
    this.carrinhoService.verificarCarrinho(produto).subscribe(dados => {
      if (dados.msg.produto === "existe") {
        this.serviceUrl.alertas(
          "Você já possui este produto em seu carrinho",
          "Para alterar qualquer caracteristica, altere diretamente na aba 'Carrinho'"
        );
        produto.checkCarrinho = false;
      } else {
        switch (produto.checkCarrinho) {
          case true:
            produto.checkCarrinho = "true";
            this.imagemCarrinho = "../../../assets/car-outline.png";
            this.selecionaCaracteristicasCarrinho(produto);

            this.carrinhoService
              .postCheckCarrinho(produto)
              .subscribe(data => {});
            break;
          case false:
            produto.checkCarrinho = "false";
            this.imagemCarrinho = "../../../assets/car-black.png";
            this.carrinhoService.deleteCarrinho(produto).subscribe(data => {});
            this.carrinhoService
              .postCheckCarrinho(produto)
              .subscribe(data => {});
        }
      }
    });
    console.log(produto.checkCarrinho);
  }

  checkFavorito(produto) {
    switch (produto.checkFavorito) {
      case true:
        produto.checkFavorito = "true";
        this.imagemFavorito = "../../../assets/heart-black.png";

        this.adicionarFavoritos();
        this.carrinhoService.postCheckFavorito(produto).subscribe(data => {});

        break;
      case false:
        produto.checkFavorito = "false";
        this.imagemFavorito = "../../../assets/heart-outline.png";

        this.carrinhoService.deleteFavorito(produto).subscribe(data => {});
        this.carrinhoService.postCheckFavorito(produto).subscribe(data => {});
    }
  }

  ngOnInit() {
    this.validarMenu();
  }
}
