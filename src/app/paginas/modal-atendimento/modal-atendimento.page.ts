import { Component, OnInit } from "@angular/core";

import { Http, Headers, Response } from "@angular/http";
import { UrlService } from "src/app/provider/url.service";
import {
  NavController,
  AlertController,
  ModalController
} from "@ionic/angular";
import { Router, ActivatedRoute } from "@angular/router";
import { MenuService } from "src/app/provider/menu.service";
import { ServiceUserService } from "src/app/provider/service-user.service";
import { map } from "rxjs/operators";

@Component({
  selector: "app-modal-atendimento",
  templateUrl: "./modal-atendimento.page.html",
  styleUrls: ["./modal-atendimento.page.scss"]
})
export class ModalAtendimentoPage implements OnInit {
  emojiExiste: any;
  imagemEmojiTriste: string;
  imagemEmojiMedio: string;
  imagemEmojiFeliz: string;
  checkEmoji1: any = false;
  checkEmoji2: any = false;
  checkEmoji3: any = false;
  emoji: any;
  codCliente: any;
  constructor(
    public http: Http,
    public serviceUrl: UrlService,
    public nav: NavController,
    private route: Router,
    public serviceUser: ServiceUserService,
    public menuService: MenuService,

    private activedRoute: ActivatedRoute,
    public alert: AlertController,
    public modalCtrl: ModalController
  ) {
    this.codCliente = this.serviceUser.getClienteId();
    this.imagemEmojiFeliz = "../../../assets/icones/feliz2.png";
    this.imagemEmojiTriste = "../../../assets/icones/ruim2.png";
    this.imagemEmojiMedio = "../../../assets/icones/ind2.png";
  }

  ngOnInit() {}

  editNotaCES() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "changeEmojiCESTrue.php?emojiSelecionado=" +
          this.emoji +
          "&codCliente=" +
          this.codCliente
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.emojiExiste = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }

  tirarNotaCES() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "changeEmojiCESFalse.php?emojiSelecionado=" +
          this.emoji +
          "&codCliente=" +
          this.codCliente
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.emojiExiste = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }

  async verificarEmoji1() {
    this.emoji = 1;

    console.log("mudou 2");
    this.imagemEmojiFeliz = "../../../assets/icones/feliz2.png";
    this.imagemEmojiMedio = "../../../assets/icones/ind2.png";

    console.log(this.imagemEmojiFeliz, this.imagemEmojiMedio);
    switch (this.checkEmoji1) {
      case true:
        this.checkEmoji1 = "true";
        this.imagemEmojiTriste = "../../../assets/icones/ruim1.png";
        this.verificarCES(this.checkEmoji1).subscribe(data => {
          if (data.dados === 0) {
            this.adicionarNotaCES();
          } else {
            this.editNotaCES();
          }
        });

        break;
      case false:
        this.checkEmoji1 = "false";
        this.imagemEmojiTriste = "../../../assets/icones/ruim2.png";

        this.tirarNotaCES();
    }
    //this.nav.navigateForward("tabs/produtos");
  }

  async verificarEmoji2() {
    this.emoji = 2;

    console.log("mudou 2");
    this.imagemEmojiFeliz = "../../../assets/icones/feliz2.png";
    this.imagemEmojiTriste = "../../../assets/icones/ruim2.png";

    console.log(this.imagemEmojiFeliz, this.imagemEmojiTriste);
    switch (this.checkEmoji2) {
      case true:
        this.checkEmoji2 = "true";
        this.imagemEmojiMedio = "../../../assets/icones/ind1.png";
        this.verificarCES(this.emoji).subscribe(data => {
          if (data.dados === 0) {
            this.adicionarNotaCES();
          } else {
            this.editNotaCES();
          }
        });

        break;
      case false:
        this.checkEmoji2 = "false";
        this.imagemEmojiMedio = "../../../assets/icones/ind2.png";

        this.tirarNotaCES();
    }
    //this.nav.navigateForward("tabs/produtos");
  }

  async verificarEmoji3() {
    this.emoji = 3;

    console.log("mudou 3");
    this.imagemEmojiTriste = "../../../assets/icones/ruim2.png";
    this.imagemEmojiMedio = "../../../assets/icones/ind2.png";

    console.log(this.imagemEmojiTriste, this.imagemEmojiMedio);
    switch (this.checkEmoji3) {
      case true:
        this.checkEmoji3 = "true";
        this.imagemEmojiFeliz = "../../../assets/icones/feliz1.png";
        this.verificarCES(this.emoji).subscribe(data => {
          if (data.dados === 0) {
            this.adicionarNotaCES();
          } else {
            this.editNotaCES();
          }
        });
        break;
      case false:
        this.checkEmoji3 = "false";
        this.imagemEmojiFeliz = "../../../assets/icones/feliz2.png";

        this.tirarNotaCES();
        break;
    }
    //this.nav.navigateForward("tabs/produtos");
  }

  verificarCES(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.serviceUrl.getUrlEstoque() +
          "verificarCES.php?emojiSelecionado=" +
          this.emoji +
          "&codCliente=" +
          this.codCliente,
        valor,
        {
          headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  adicionarNotaCES() {
    this.serviceUrl.exibirLoading();
    this.http
      .get(
        this.serviceUrl.getUrlEstoque() +
          "adicionarEmojiCES.php?emojiSelecionado=" +
          this.emoji +
          "&codCliente=" +
          this.codCliente
      )
      .pipe(map(res => res.json()))
      .subscribe(getQuantidade => {
        console.log(getQuantidade.dados);
        this.emojiExiste = getQuantidade.dados;
      });
    this.serviceUrl.fecharLoding();
  }

  continueComprando() {
    this.nav.navigateForward("tabs/produtos");
  }
}
