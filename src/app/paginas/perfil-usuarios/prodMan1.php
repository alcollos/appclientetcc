<?php
// Pasta onde o arquivo vai ser salvo
$_UP['pasta'] = 'uploads/';
// Tamanho m�ximo do arquivo (em Bytes)
$_UP['tamanho'] = 1024 * 1024 * 2; // 2Mb
// Array com as extens�es permitidas
$_UP['extensoes'] = array('jpg', 'png', 'gif', 'doc', 'docx', 'xlsx', 'xls');
// Renomeia o arquivo? (Se true, o arquivo ser� salvo como .jpg e um nome �nico)
$_UP['renomeia'] = true;


// Array com os tipos de erros de upload do PHP
$_UP['erros'][0] = 'N�o houve erro';
$_UP['erros'][1] = 'O arquivo no upload � maior do que o limite do PHP';
$_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML';
$_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente';
$_UP['erros'][4] = 'N�o foi feito o upload do arquivo';


// Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
if ($_FILES['arquivo']['error'] != 0)
{
  die("N�o foi poss�vel fazer o upload, erro:<br />" . $_UP['erros'][$_FILES['arquivo']['error']]);
  exit; // Para a execu��o do script
}

// Consegue o nome do arquivo sem a extens�o
$tmp = explode('.', $_FILES['arquivo']['name']);
$sonome = strtolower(reset($tmp));

// Verifica a extens�o do arquivo
$extensao = strtolower(end($tmp));
if (array_search($extensao, $_UP['extensoes']) === false)
{
  echo "Por favor, envie arquivos com as seguintes extens�es: jpg, png, gif, doc, docx, xlsx, xls";
}
else
{
  if ($_UP['tamanho'] < $_FILES['arquivo']['size'])
  {
    echo "O arquivo enviado � muito grande, envie arquivos de at� 2Mb.";
  }
  else
  {
    $nome_original = $_FILES['arquivo']['name'];
    if ($_UP['renomeia'] == true)
    {
      // Coloca o TimeStamp
      $nome_final = time().'@'.$sonome.'.'.$extensao;
    }
    else
    {
      // Mant�m o nome original do arquivo
      $nome_final = $_FILES['arquivo']['name'];
    }
  }

  // Depois verifica se � poss�vel mover o arquivo para a pasta escolhida
  if (move_uploaded_file($_FILES['arquivo']['tmp_name'], $_UP['pasta'] . $nome_final))
  {
     // Upload efetuado com sucesso, exibe uma mensagem e um link para o arquivo
     echo "<BR>Upload efetuado com sucesso!";
     echo "<BR>Nome Original: $nome_original";
     echo "<BR>Nome Final: $nome_final";
     echo '<br /><a href="' . $_UP['pasta'] . $nome_final . '">Clique aqui para acessar o arquivo</a>';
  }
  else
  {
     // N�o foi poss�vel fazer o upload, provavelmente a pasta est� incorreta
     echo "N�o foi poss�vel enviar o arquivo, tente novamente";
  }
}

?>
