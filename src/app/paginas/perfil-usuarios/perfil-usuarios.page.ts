import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import {
  AlertController,
  NavController,
  LoadingController,
  Platform
} from "@ionic/angular";
import { UrlService } from "../../provider/url.service";
import { MenuService } from "../../provider/menu.service";
import { map } from "rxjs/operators";
import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { NFC, Ndef } from "@ionic-native/nfc/ngx";
import { ServiceUserService } from "../../provider/service-user.service";
import { Router, ActivatedRoute, NavigationExtras } from "@angular/router";
import {
  BarcodeScannerOptions,
  BarcodeScanner
} from "@ionic-native/barcode-scanner/ngx";
import {
  CameraPreview,
  CameraPreviewOptions,
  CameraPreviewPictureOptions
} from "@ionic-native/camera-preview/ngx";
import { Validators, FormGroup, FormBuilder } from "@angular/forms";
import { Subscription } from "rxjs";
import { ValueAccessor } from "@ionic/angular/dist/directives/control-value-accessors/value-accessor";
import { CarrinhoServiceService } from "src/app/provider/carrinho/carrinho-service.service";

@Component({
  selector: "app-perfil-usuarios",
  templateUrl: "./perfil-usuarios.page.html",
  styleUrls: ["./perfil-usuarios.page.scss"]
})
export class PerfilUsuariosPage implements OnInit {
  codCliente: any;
  detalhesCliente: Array<{
    nome: any;
    endereco: any;
    telefone: any;
    email: any;
    dataNascimento: any;
    foto: any;
  }>;

  nomeCliente: any;
  enderecoCliente: any;
  telefoneCliente: any;
  emailCliente: any;
  nascimentoCliente: any;
  imgPerfil: any;

  cliente: any;
  constructor(
    public alert: AlertController,
    public urlService: UrlService,
    public http: Http,
    public nav: NavController,
    public loading: LoadingController,
    public serviceUser: ServiceUserService,
    public ativeRouter: ActivatedRoute,
    public serviceUrl: UrlService,
    public barcodeScanner: BarcodeScanner,
    public navController: NavController,
    public menuService: MenuService,
    public nfce: NFC,
    public frmBuilder: FormBuilder,
    public camerapreview: CameraPreview,
    public platform: Platform,
    public ndef: Ndef,

    public carrinhoService: CarrinhoServiceService
  ) {
    this.listCliente();
    this.nomeCliente = this.serviceUser.getClienteNome();

    this.emailCliente = this.serviceUser.getClienteEmail();

    this.imgPerfil = urlService.url + this.serviceUser.getClienteFoto();

    //this.imgPerfil = "../../../assets/luan.jpeg";

    this.cliente = this.frmBuilder.group({
      codCliente: this.serviceUser.getClienteId(),
      nomeCliente: ["", Validators.required],
      emailCliente: ["", Validators.required],
      telefoneCliente: ["", Validators.required],
      nascimentoCliente: ["", Validators.required],
      enderecoCliente: ["", Validators.required]
    });
  }

  listCliente() {
    this.codCliente = this.serviceUser.getClienteId();
    this.detalhesCliente = [];
    this.http
      .get(
        this.urlService.getUrlEstoque() +
          "detalhesCliente.php?idcliente=" +
          this.codCliente
      )
      .pipe(map(res => res.json()))
      .subscribe(data => {
        for (let i = 0; i < data.length; i++) {
          this.detalhesCliente.push({
            nome: data[i].nome,
            endereco: data[i].endereco,
            telefone: data[i].telefone,
            email: data[i].email,
            dataNascimento: data[i].dataNascimento,
            foto: data[i].foto
          });
        }
        this.enderecoCliente = this.detalhesCliente[0].endereco;
        this.telefoneCliente = this.detalhesCliente[0].telefone;
        this.nascimentoCliente = this.detalhesCliente[0].dataNascimento;
      });
  }

  alterarSenha() {
    this.nav.navigateForward("alterar-senha");
  }
  ngOnInit() {}

  updateClienteNome() {
    console.log(this.cliente.value.nomeCliente);

    this.postClienteNome(this.cliente.value.nomeCliente).subscribe(data => {});
  }

  updateClienteEmail() {
    console.log(this.cliente.value.nomeCliente);

    this.postClienteEmail(this.cliente.value.emailCliente).subscribe(
      data => {}
    );
  }

  updateClienteTelefone() {
    console.log(this.cliente.value.nomeCliente);

    this.postClienteTelefone(this.cliente.value.telefoneCliente).subscribe(
      data => {}
    );
  }

  updateClienteNascimento() {
    console.log(this.cliente.value.nomeCliente);

    this.postClienteNascimento(this.cliente.value.nascimentoCliente).subscribe(
      data => {}
    );
  }

  updateClienteEndereco() {
    console.log(this.cliente.value.nomeCliente);

    this.postClienteEndereco(this.cliente.value.enderecoCliente).subscribe(
      data => {}
    );
  }

  postClienteNome(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.urlService.getUrlEstoque() +
          "atualizarClienteNome.php?codCliente=" +
          this.serviceUser.getClienteId(),
        valor,
        {
          headers: headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  postClienteEmail(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.urlService.getUrlEstoque() +
          "atualizarClienteEmail.php?codCliente=" +
          this.serviceUser.getClienteId(),
        valor,
        {
          headers: headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  postClienteTelefone(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.urlService.getUrlEstoque() +
          "atualizarClienteTelefone.php?codCliente=" +
          this.serviceUser.getClienteId(),
        valor,
        {
          headers: headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  postClienteEndereco(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.urlService.getUrlEstoque() +
          "atualizarClienteEndereco.php?codCliente=" +
          this.serviceUser.getClienteId(),
        valor,
        {
          headers: headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  postClienteNascimento(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.urlService.getUrlEstoque() +
          "atualizarClienteNascimento.php?codCliente=" +
          this.serviceUser.getClienteId(),
        valor,
        {
          headers: headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }
}
