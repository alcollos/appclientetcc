import { Component, OnInit } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";

// tslint:disable-next-line: quotemark
import { NavController, AlertController } from "@ionic/angular";
import { Router } from "@angular/router";

import { ServiceUserService } from "src/app/provider/service-user.service";
import { map } from "rxjs/operators";

import { CarrinhoServiceService } from "src/app/provider/carrinho/carrinho-service.service";
import { UrlService } from "src/app/provider/url.service";
import { MenuService } from "src/app/provider/menu.service";
import { ProdutosPage } from "../produtos/produtos.page";

@Component({
  selector: "app-carrinho",
  templateUrl: "./carrinho.page.html",
  styleUrls: ["./carrinho.page.scss"]
})
export class CarrinhoPage implements OnInit {
  CarrinhoItem: Array<{
    codigos: any;
    selecionados: any;
    // Variaveis utilizadas para captar qual o valor que foi selecionado no select
    corSelecionado: any;
    // quantidadeSelecionado: any;
    tamanhoSelecionado: any;
    codCliente: any;
    cor: any;
    categorias: any;
    fornecedores: any;
    marcas: any;
    modelos: any;
    palmilhas: any;
    pesos: any;
    tamanhos: any;
    promocoes: any;
    valor: any;
    qtd: any;
    descricao: any;
    foto: any;
    cod_cor: any;
    // cod_qtd: any;
    cod_tamanho: any;
  }>;

  aTamanho: Array<{
    cod_tamanho: any;
    tamanho: any;
  }>;

  aCor: Array<{
    cod_cor: any;
    cor: any;
  }>;

  /*aQuantidade: Array<{
    cod_qtd: any;
    quantidade: any;
  }>;*/

  // tslint:disable-next-line: variable-name
  cod_prod: any;

  tamanho: any;
  teste: any;
  cbAtivo: boolean;

  constructor(
    public http: Http,
    public serviceUrl: UrlService,
    public nav: NavController,
    private route: Router,
    public serviceUser: ServiceUserService,
    public menuService: MenuService,
    public carrinhoService: CarrinhoServiceService,
    public alert: AlertController
  ) {
    this.menuService.validarMenu();
    this.listCarrinho();
    this.listTamanho();
    this.cbAtivo = false;
    // this.listQuantidade();
    this.listCor();
  }

  solicitarProdutos(produtos) {
    produtos = this.serviceUser.getClienteId();
    this.carrinhoService.postSelecionados(produtos).subscribe(dados => {
      if (dados.msg.produto === "existe") {
        this.serviceUrl.alertaConfirmacaoPedido();
        this.postCliente(this.serviceUser.getClienteId()).subscribe(data => {});
      } else {
        this.serviceUrl.alertas(
          "ATENÇÃO !!",
          "Você não selecionou nenhum produto"
        );
      }
    });
  }

  listCarrinho() {
    this.CarrinhoItem = [];
    this.serviceUrl.exibirLoading();

    this.http
      .get(this.serviceUrl.getUrlCarrinho() + "listCarrinho.php")
      .pipe(map(res => res.json()))
      .subscribe(data => {
        // tslint:disable-next-line: prefer-for-of
        for (let i = 0; i < data.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          if (data[i].cliente === this.serviceUser.idCliente) {
            {
              this.CarrinhoItem.push({
                codigos: data[i].codigo,
                modelos: data[i].modelo,
                valor: data[i].valor,
                foto: data[i].foto,
                qtd: data[i].quantidade,
                descricao: data[i].descricao,
                cor: data[i].cor,
                categorias: data[i].categoria,
                fornecedores: data[i].fornecedor,
                marcas: data[i].marca,
                palmilhas: data[i].palmilha,
                pesos: data[i].peso,
                tamanhos: data[i].tamanho,
                promocoes: data[i].promocao,
                selecionados: data[i].selecionado,
                cod_tamanho: data[i].codTamanho,
                cod_cor: data[i].codCor,

                codCliente: this.serviceUser.getClienteId(),
                // cod_qtd: data[i]["codQtd"],

                // Variaveis utilizadas para captar qual o valor que foi selecionado no select
                tamanhoSelecionado: data[i].selecao,
                corSelecionado: data[i].selecao
                // quantidadeSelecionado: data[i].selecao
              });
              console.log(this.CarrinhoItem);
            }
          }
        }

        error => {
          this.serviceUrl.alertas(
            "Atenção",
            "não possível carregar os produtos, verifique sua conexão!"
          );
          this.serviceUrl.fecharLoding();
        };
      });
    this.serviceUrl.fecharLoding();
  }

  async deleteProduto(produto) {
    const alert = await this.alert.create({
      header: "Atenção",
      message: "Você realmente deseja excluir esse produto?",
      buttons: [
        {
          text: "Não",
          role: "cancel",
          handler: () => {
            console.log("Clicou no Cancelar");
          }
        },
        {
          text: "Sim",
          handler: () => {
            console.log("Clicou no Alterar");
            this.carrinhoService.deleteCarrinho(produto).subscribe(
              data => {
                console.log("produto deletado com sucesso");
                this.listCarrinho();
              },
              error => {
                console.log("erro ao tentar excluir" + error);
              }
            );
          }
        }
      ]
    });
    await alert.present();
  }

  selecionarItem(produto) {
    switch (produto.selecionados) {
      case true:
        produto.selecionados = "true";
        this.selecionaQuantidade(produto);
        this.cbAtivo = true;
        break;
      case false:
        produto.selecionados = "false";
        this.cbAtivo = false;
    }
    console.log(this.CarrinhoItem);

    this.postSelecionado(produto).subscribe(data => {});
  }

  selecionaQuantidade(produto) {
    produto.tamanhoSelecionado = produto.cod_tamanho;
    produto.corSelecionado = produto.cod_cor;
    // produto.cod_qtd = produto.qtdSelecionado;
    this.carrinhoService
      .postCarrinhoxCaracteristicas(produto)
      .subscribe(dados => {
        if (dados.msg.produto === "existe") {
          this.updateCaracteristicas(produto).subscribe(data => {});
        } else {
          this.serviceUrl.alertas(
            "Desculpe",
            " Não possuimos um " +
              produto.modelos +
              " com essa combinação de Tamanho e Cor "
          );
          produto.selecionados = false;
          console.log("oi");
        }
      });
  }

  postSelecionado(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.serviceUrl.getUrlCarrinho() + "updateCheckboxSelecionado.php",
        valor,
        {
          headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  postCliente(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.serviceUrl.getUrlCarrinho() + "adicionarCliente.php", valor, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  doRefresh(event) {
    console.log("Begin async operation");
    this.listCarrinho();
    this.listTamanho();
    this.listCor();
    // this.listQuantidade();

    setTimeout(() => {
      console.log("Async operation has ended");
      event.target.complete();
      console.log(event.target.complete());
    }, 2000);
  }

  /* mudaTamanho() {
    console.log(this.cod_prod);

    this.postSelecionado(this.aTamanho).subscribe(
      data => {
        console.log('Inserido com sucesso');
      },
      err => {
        console.log('erro ao tentar inserir');
      }
    );

  }*/

  listTamanho() {
    console.log(this.aTamanho);
    this.aTamanho = [];
    this.http
      .get(this.serviceUrl.getUrlSelect() + "listTamanho.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.aTamanho.push({
            cod_tamanho: dados[i].codigo,
            tamanho: dados[i].tamanho
          });
        }
      });
  }

  listCor() {
    this.aCor = [];
    this.http
      .get(this.serviceUrl.getUrlSelect() + "listCor.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.aCor.push({
            cod_cor: dados[i].codigo,
            cor: dados[i].cor
          });
        }
      });
  }

  /*listQuantidade() {
    this.aQuantidade = [];
    this.http
      .get(this.serviceUrl.getUrlSelect() + "listQuantidade.php")
      .pipe(map(res => res.json()))
      .subscribe(dados => {
        for (let i = 0; i < dados.length; i++) {
          // Se o id do banco, for igual ao ID do usário logado
          this.aQuantidade.push({
            cod_qtd: dados[i]["codigo"],
            quantidade: dados[i]["quantidade"]
          });
        }
      });
  }*/

  updateCaracteristicas(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.serviceUrl.getUrlEstoque() + "updateCaracteristicas.php",
        valor,
        {
          headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  editQuantidade(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.serviceUrl.getUrlEstoque() + "editQuantidade.php", valor, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  editCor(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.serviceUrl.getUrlEstoque() + "editQuantidade.php", valor, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  editTamanho(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.serviceUrl.getUrlEstoque() + "editQuantidade.php", valor, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  ngOnInit() {
    this.cbAtivo = false;
    console.log(this.cbAtivo);
  }
}
