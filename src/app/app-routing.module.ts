import { NgModule } from "@angular/core";
import { Routes, RouterModule, PreloadAllModules } from "@angular/router";

const routes: Routes = [
  {
    path: "",
    redirectTo: "login",
    pathMatch: "full"
  },

  {
    path: "login",
    loadChildren: "./paginas/login/login.module#LoginPageModule"
  },
  {
    path: "introducao",
    loadChildren: "./paginas/introducao/introducao.module#IntroducaoPageModule"
  },

  {
    path: "cadastrar-usuarios",
    loadChildren:
      "./paginas/cadastrar-usuarios/cadastrar-usuarios.module#CadastrarUsuariosPageModule"
  },

  { path: "", loadChildren: "./tabs/tabs.module#TabsPageModule" },
  {
    path: "codigoCliente",
    loadChildren:
      "./paginas/codigo-cliente/codigo-cliente.module#CodigoClientePageModule"
  },
  {
    path: "finalizacao",
    loadChildren:
      "./paginas/finalizacao/finalizacao.module#FinalizacaoPageModule"
  },
  {
    path: "branca",
    loadChildren: "./paginas/branca/branca.module#BrancaPageModule"
  },
  {
    path: "pesquisa-satisfacao",
    loadChildren:
      "./paginas/pesquisa-satisfacao/pesquisa-satisfacao.module#PesquisaSatisfacaoPageModule"
  },
  {
    path: "perfil-usuarios",
    loadChildren:
      "./paginas/perfil-usuarios/perfil-usuarios.module#PerfilUsuariosPageModule"
  },
  {
    path: "grafico-emoji",
    loadChildren:
      "./paginas/grafico-emoji/grafico-emoji.module#GraficoEmojiPageModule"
  },
  {
    path: "alterar-senha",
    loadChildren:
      "./paginas/alterar-senha/alterar-senha.module#AlterarSenhaPageModule"
  },
  { path: 'modal-atendimento', loadChildren: './paginas/modal-atendimento/modal-atendimento.module#ModalAtendimentoPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
