import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class ServiceUserService {
  idCliente = "";
  clienteNome = "";
  clienteEmail = "";
  clienteTelefone = "";
  clienteCPF = "";
  clienteRG = "";
  clienteEndereco = "";
  clienteSenha = "";
  clienteStatus = "";
  clienteFoto: any;

  constructor() {}

  // Esse dados estão sendo pegos na tela de login
  setClienteNome(valor) {
    this.clienteNome = valor;
  }

  getClienteNome() {
    return this.clienteNome;
  }

  // GET; SET; COD_FUNC
  setClienteId(valor) {
    this.idCliente = valor;
  }

  getClienteId() {
    return this.idCliente;
  }

  // GET; SET; STATUS
  setClienteStatus(valor) {
    this.clienteStatus = valor;
  }

  geClienteStatus() {
    return this.clienteStatus;
  }

  // GET; SET; FOTO
  setClienteFoto(valor) {
    this.clienteFoto = valor;
  }

  getClienteFoto() {
    return this.clienteFoto;
  }

  // GET; SET; EMAIL
  setClienteEmail(valor) {
    this.clienteEmail = valor;
  }

  getClienteEmail() {
    return this.clienteEmail;
  }

  // GET; SET;TELEFONE
  setClienteTelefone(valor) {
    this.clienteTelefone = valor;
  }

  getClienteTelefone() {
    return this.clienteTelefone;
  }

  // GET; SET; cpf
  setClienteCPF(valor) {
    this.clienteCPF = valor;
  }

  getClienteCPF() {
    return this.clienteCPF;
  }

  // GET; SET; RG
  setClienteRG(valor) {
    this.clienteRG = valor;
  }

  getClienteRG() {
    return this.clienteRG;
  }

  // GET; SET; ENDERECO
  setClienteEndereco(valor) {
    this.clienteEndereco = valor;
  }

  getClienteEndereco() {
    return this.clienteEndereco;
  }

  // GET; SET; ENDERECO
  setClienteSenha(valor) {
    this.clienteSenha = valor;
  }

  getClienteSenha() {
    return this.clienteSenha;
  }
}
