import { Injectable } from "@angular/core";
import {
  AlertController,
  LoadingController,
  NavController
} from "@ionic/angular";
import { analyzeAndValidateNgModules } from "@angular/compiler";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class UrlService {
  //url: string = 'http://teratags.com/tcc/php/';

  url_login: string = "http://teratags.com/tcc/php/";
  url_carrinho: string = "http://teratags.com/tcc/php/";
  url_estoque: string = "http://teratags.com/tcc/php/";
  url_cadastro: string = "http://teratags.com/tcc/php/";
  url_select: string = "http://teratags.com/tcc/php/";
  url_favorito: string = "http://teratags.com/tcc/php/";
  url: string = "http://teratags.com/tcc/php/img_perfil/";
  urlFoto: string = "http://teratags.com/tcc/php/img_produtos/";
  url_teste: string = "http://localhost/appCliente/php/teste_local/";

  /*
  url_login: string = 'http://localhost/appCliente/php/funcao_login/';
  url_carrinho: string = 'http://localhost/appCliente/php/funcoes_carrinho/';
  url_estoque: string = 'http://localhost/appCliente/php/funcoes_estoque/';
  url_cadastro: string = 'http://localhost/appCliente/php/funcoes_cadastro/';
  url_select: string = 'http://localhost/appCliente/php/funcoes_select/';
  url_favorito: string = 'http://localhost/appCliente/php/funcao_favorito/';
  url: string = 'http://localhost/appCliente/php/';
*/
  loading = false;

  constructor(
    public alert: AlertController,
    public loadingCrtl: LoadingController,
    public nav: NavController,
    private route: Router
  ) {}
  getUrlSelect() {
    return this.url_select;
  }

  getUrlLogin() {
    return this.url_login;
  }
  getUrlCarrinho() {
    return this.url_carrinho;
  }
  getUrlEstoque() {
    return this.url_estoque;
  }
  getUrlCadastro() {
    return this.url_cadastro;
  }

  getUrlFavorito() {
    return this.url_favorito;
  }

  getUrlTeste() {
    return this.url_teste;
  }
  async alertas(titulo, msg) {
    const alert = await this.alert.create({
      header: titulo,
      message: msg,
      buttons: ["OK"]
    });
    await alert.present();
  }

  async alertaConfirmacaoPedido() {
    const alert = await this.alert.create({
      header: "Atenção",
      message: "Você realmente deseja pedir esses produtos?",
      buttons: [
        {
          text: "Não",
          role: "cancel",
          handler: () => {
            console.log("Clicou no Cancelar");
          }
        },
        {
          text: "Sim",
          handler: () => {
            console.log("Clicou no Alterar");
            this.nav.navigateForward("codigoCliente");
          }
        }
      ]
    });
    await alert.present();
  }

  async alertaCadastro() {
    const alert = await this.alert.create({
      header: "Parabéns",
      message: "Seu cadastro foi efetuado",
      buttons: [
        {
          text: "OK",

          handler: () => {
            console.log("Clicou no Cancelar");
            this.nav.navigateForward("tabs/produtos");
          }
        }
      ]
    });
    await alert.present();
  }

  async alertaConfirmacaoCompra() {
    const alert = await this.alert.create({
      header: "Atenção",
      message: "Você realmente deseja comprar esses produtos?",
      buttons: [
        {
          text: "Não",
          role: "cancel",
          handler: () => {
            console.log("Clicou no Cancelar");
          }
        },
        {
          text: "Sim",
          handler: () => {
            console.log("Clicou no Alterar");

            let paginas = {
              queryParams: {
                pagina: 1
              }
            };
            this.route.navigate(["codigoCliente"], paginas);
          }
        }
      ]
    });
    await alert.present();
  }

  async exibirLoading() {
    this.loading = true;
    return await this.loadingCrtl
      .create({
        message: "Buscando dados..."
      })
      .then(a => {
        a.present().then(() => {
          if (!this.loading) {
            a.dismiss().then(() => {});
          }
        });
      });
  }

  async fecharLoding() {
    this.loading = false;
    return await this.loadingCrtl.dismiss().then(() => {});
  }
}
