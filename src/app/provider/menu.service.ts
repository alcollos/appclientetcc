import { Injectable } from "@angular/core";
import { Component, OnInit } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";
import { UrlService } from "src/app/provider/url.service";
import { NavController } from "@ionic/angular";
import { Router } from "@angular/router";
import { ServiceUserService } from "./service-user.service";

@Injectable({
  providedIn: "root"
})
export class MenuService {
  // Cria os espaços de opção do menu lateral
  public menu = [
    {
      label: "Escolha de Produtos",
      icone: "appstore",
      acao: "produtos",
      menu: "0",
      exibirMenu: true
    },
    {
      label: "Meu perfil",
      icone: "contact",
      acao: "perfil",
      menu: "1",
      exibirMenu: true
    }
  ];

  public perfilMenu = [];

  constructor(
    public http: Http,
    public serviceUrl: UrlService,
    public nav: NavController,
    private route: Router,

    public serviceUser: ServiceUserService
  ) {}

  validarMenu() {
    if (localStorage.getItem("user_logado") != null) {
      console.log("usuario logado");

      this.serviceUser.setClienteId(localStorage.getItem("idCliente"));
      this.serviceUser.setClienteNome(localStorage.getItem("NameCliente"));
      this.serviceUser.setClienteStatus(localStorage.getItem("statusCliente"));
    } else {
      this.nav.navigateBack("login");
    }
  }
}
