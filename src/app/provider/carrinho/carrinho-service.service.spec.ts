import { TestBed } from '@angular/core/testing';

import { CarrinhoServiceService } from './carrinho-service.service';

describe('CarrinhoServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CarrinhoServiceService = TestBed.get(CarrinhoServiceService);
    expect(service).toBeTruthy();
  });
});
