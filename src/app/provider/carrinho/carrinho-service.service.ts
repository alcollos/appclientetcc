import { Injectable } from "@angular/core";
import { Component, OnInit } from "@angular/core";

import { Http, Headers, Response, ResponseOptions } from "@angular/http";
import { map } from "rxjs/operators";
import { UrlService } from "../../provider/url.service";
import { NavController, Platform } from "@ionic/angular";
import { ServiceUserService } from "../../provider/service-user.service";
import { NFC, Ndef } from "@ionic-native/nfc/ngx";
import { ActivatedRoute } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class CarrinhoServiceService {
  aCor: Array<{
    codigo: any;
    nome: any;
  }>;

  constructor(
    public navCtrl: NavController,
    public http: Http,
    private activedRoute: ActivatedRoute,
    public urlService: UrlService,
    public serviceUser: ServiceUserService,
    public nfce: NFC,
    public serviceUrl: UrlService,

    public platform: Platform,
    public ndef: Ndef
  ) {}

  postCaracteristicas(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.serviceUrl.getUrlEstoque() + "vProdutoxCaracteristicas.php",
        valor,
        {
          headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  postCompra(valor) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.serviceUrl.getUrlEstoque() + "selecionadosCompra.php", valor, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  verificarCarrinho(valor) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.serviceUrl.getUrlEstoque() + "verificarCarrinho.php", valor, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  postCarrinhoxCaracteristicas(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.serviceUrl.getUrlEstoque() + "vCarrinhoxCaracteristicas.php",
        valor,
        {
          headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  postSelecionados(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.serviceUrl.getUrlEstoque() + "selecionadosCarrinho.php",
        valor,
        {
          headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }
  verificarCadastro(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.serviceUrl.getUrlEstoque() + "cadastroExistente.php", valor, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  deleteCarrinho(codigo) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.serviceUrl.getUrlCarrinho() + "deleteCarrinho.php", codigo, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res;
        })
      );
  }

  deleteFavorito(codigo) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.serviceUrl.getUrlCarrinho() + "deleteFavoritos.php", codigo, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res;
        })
      );
  }

  postCheckCarrinho(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.serviceUrl.getUrlEstoque() + "updateCheckCarrinho.php",
        valor,
        {
          headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  postCheckFavorito(valor) {
    console.log("atualizar");

    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.serviceUrl.getUrlEstoque() + "updateCheckFavorito.php",
        valor,
        {
          headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  updateFalse(valor) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(this.serviceUrl.getUrlEstoque() + "changeFalse.php", valor, {
        headers,
        method: "POST"
      })
      .pipe(
        map((res: Response) => {
          return res.json();
        })
      );
  }

  deleteCarAll(codigo) {
    const headers = new Headers({
      "Content-Type": "application/x-www-form-urlencoded"
    });
    return this.http
      .post(
        this.serviceUrl.getUrlCarrinho() + "deleteCARRINHOALL.php",
        codigo,
        {
          headers,
          method: "POST"
        }
      )
      .pipe(
        map((res: Response) => {
          return res;
        })
      );
  }
}
