import { Component } from "@angular/core";
import { Routes } from "@angular/router";
import { Platform, NavController } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { ServiceUserService } from "../app/provider/service-user.service";
import { UrlService } from "../app/provider/url.service";
import { MenuService } from "../app/provider/menu.service";
import { CarrinhoPage } from "./paginas/carrinho/carrinho.page";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html"
})
export class AppComponent {
  public appPages = [
    {
      title: "Home",
      url: "/home",
      icon: "home"
    },
    {
      title: "List",
      url: "/list",
      icon: "list"
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public seriveUser: ServiceUserService,
    public urlService: UrlService,
    public navController: NavController,
    public menuService: MenuService
  ) {
    this.seriveUser.getClienteNome();
    this.seriveUser.getClienteFoto();
    this.urlService.getUrlCadastro();
    this.urlService.getUrlCarrinho();
    this.urlService.getUrlLogin();
    this.urlService.getUrlEstoque();

    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  // Para redirecionar para uma pagina em especifico
  acaoMenu(menu) {
    switch (menu) {
      case "produtos":
        this.produto();
        break;
      case "perfil":
        this.perfil();
        break;

      default:
        break;
    }
  }

  // Pagina que serão redirecionadas

  produto() {
    this.navController.navigateForward("tabs/produtos");
  }
  perfil() {
    this.navController.navigateForward("perfil-usuarios");
  }

  // logout
  sair() {
    localStorage.clear();
    location.reload();
    this.navController.navigateRoot("login");
    localStorage.setItem("deslogado", "sim");
  }
}
